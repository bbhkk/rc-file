call plug#begin('~/.vim/plugged')

Plug 'peterhoeg/vim-qml'
"Plug 'scrooloose/nerdcommenter'
Plug 'tpope/vim-commentary'

call plug#end()


set nu
set ru
color desert

syntax on

set sw=0  " shiftwidth follows tabstop
"set ts=4  " tabstop = 4 char
set ts=2
set et    " enable expand tab: tab to spaces
set ai    " enable autoindent: copy indent from previous line

set fo=roqtc

set ve=block

set backspace=2  " backspace over everything

set hls   " enable highligh on search
set ls=2  " alway show status line, ls=laststatus

set clipboard^=unnamed,unnamedplus

set mouse=a
set ttym=xterm2

set showtabline=2  " always show tabline

filetype plugin on
filetype indent off
set omnifunc=syntaxcomplete#Complete

set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:␣
"set listchars+=space:␣
"set list

" allows ctrl x,c,v to work
vmap <c-c> "+yi
vmap <c-x> "+c
vmap <c-v> c<esc>"+p
"imap <c-v> <esc>"+pa

nnoremap <leader>cd :cd %:p:h